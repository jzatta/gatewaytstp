
#include "SSLX509.h"

#include "ca_bundle.h"
#include "Log.h"

#include <stdlib.h>
#include <new>


SSLX509 *SSLX509::singleton = NULL;
int SSLX509::activeUsages = 0;

SSLX509::SSLX509(void) {
  // Initialize certificates
  init();
  activeUsages = 0;
}

SSLX509::~SSLX509(void) {
  deinit();
}

SSLX509 *SSLX509::getX509(void) {
  if (singleton == NULL) {
    singleton = new SSLX509();
  }
  singleton->activeUsages++;
  return singleton;
}

void SSLX509::destroy(void) {
  activeUsages--;
  if (activeUsages > 0) {
    return;
  }
  delete singleton;
  singleton = NULL;
}

void SSLX509::init(void) {
  int ret;
  
  mbedtls_x509_crt_init(&caCerts);
  mbedtls_x509_crt_init(&clientCerts);
  mbedtls_x509_crl_init(&revokedChain);
  mbedtls_pk_init(&pkey);
  
  
  LL_LOG( "Loading the CA root certificate");
  ret = mbedtls_x509_crt_parse(&caCerts,
                               (const unsigned char *) ca_bundle,
                                  sizeof(ca_bundle));
  if (ret != 0) {
    LL_CRITICAL("  mbedtls_x509_crt_parse returned -%#x", -ret);
    return;
  }
//   TODO Check certificates revoked list
//   https://tls.mbed.org/api/group__x509__module.html#details
  
  
}

void SSLX509::deinit(void) {
  mbedtls_x509_crt_free(&caCerts);
  mbedtls_x509_crt_free(&clientCerts);
  mbedtls_pk_free(&pkey);
  mbedtls_x509_crl_free(&revokedChain);
}

void SSLX509::ssl_conf_ca_chain(mbedtls_ssl_config *conf) {
  mbedtls_ssl_conf_ca_chain(conf, &caCerts, &revokedChain);
}


int SSLX509::ssl_conf_own_cert(mbedtls_ssl_config *conf) {
  mbedtls_ssl_conf_own_cert(conf, &clientCerts, &pkey);
}


int SSLX509::ssl_conf(mbedtls_ssl_config *conf) {
  ssl_conf_ca_chain(conf);
  return ssl_conf_own_cert(conf);
}
