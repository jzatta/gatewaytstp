
#include "Enclave_t.h"
#include "Log.h"

#include <string.h> 

#include "SSLConnection.h"

SSLConnection *conn;

int sgx_connect() {
  unsigned char buf[1024];
  const char *test, *exit;
#if 1
  exit = "exit";
test = 
"POST /api/get.php HTTP/1.1\r\n"
"User-Agent: node-XMLHttpRequest\r\n"
"Accept: */*\r\n"
"Host: iot.lisha.ufsc.br\r\n"
"Content-Length: 175\r\n"
"Content-Type: text/plain;charset=UTF-8\r\n"
"Connection: close\r\n\r\n";


const char *content =
"{\"series\":{\"version\":\"1.1\",\"unit\":2224179493,\"x\":741868770,\"y\":679816011,\"z\":25285,\"r\":3000,\"t0\":1507378000000000,\"t1\":1507378987494000},\"credentials\":{\"domain\":\"smartlisha\"}}";

  
  
  SSLConnection *conn = new SSLConnection("iot.lisha.ufsc.br", "443");
  printf_sgx("connect? %d\n\n", conn->connect());
  printf_sgx("writen ? %d\n\n", conn->write(test, strlen(test)));
  printf_sgx("writen ? %d\n\n", conn->write(content, strlen(content)));
  printf_sgx("writen ? %d\n\n", conn->write(content, strlen(content)));
  printf_sgx("readed ? %d\n\n", conn->read(buf, sizeof(buf)));
  int i = 0;
  while (conn->read(buf, sizeof(buf)) == 1024) {
    printf_sgx("%d: s\n", i, buf);
    i++;
  }
  printf_sgx("%d: %s\n", i, buf);
  printf_sgx("\n\n%d\n\n", i);
//   printf_sgx("writen ? %d\n\n", conn->write(test, strlen(test)));
//   printf_sgx("readed ? %d\n\n", conn->read(buf, sizeof(buf)));
//   printf_sgx("writen ? %d\n\n", conn->write(exit, strlen(exit)));
#else
  test = "teste";
  SSLConnection *conn = new SSLConnection("127.0.0.1", "8443");
  printf_sgx("connect? %d\n\n", conn->connect());
  printf_sgx("writen ? %d\n\n", conn->write(test, strlen(test)));
  printf_sgx("readed ? %d\n\n", conn->read(buf, sizeof(buf)));
#endif
  conn->close();
  delete conn;
  return 0;
}
