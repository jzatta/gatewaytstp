
#pragma once

#include "ca_bundle.h"
#include "mbedtls/ssl.h"
#include "mbedtls/x509.h"

// This lib is not thread safe

class SSLX509 {
private:
  static SSLX509 *singleton;
  static int activeUsages;
  
  mbedtls_x509_crt caCerts;
  mbedtls_x509_crt clientCerts;
  mbedtls_x509_crl revokedChain;
  mbedtls_pk_context pkey;
  
  void init(void);
  void deinit(void);
  
  SSLX509();
  ~SSLX509();

public:
  static SSLX509 *getX509(void);
  static void destroy(void);
  void ssl_conf_ca_chain(mbedtls_ssl_config *conf);
  int ssl_conf_own_cert(mbedtls_ssl_config *conf);
  int ssl_conf(mbedtls_ssl_config *conf);
};
