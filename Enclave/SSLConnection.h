
#pragma once

#include "SSLX509.h"
#include "mbedtls/ssl.h"

#include "mbedtls/net_v.h"
#include "mbedtls/net_f.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/certs.h"
#include "mbedtls/x509.h"
#include "mbedtls/error.h"
#include "mbedtls/debug.h"

#include "Enclave_t.h"
#include "Log.h"
#include "pprint.h"

#define SERVER    127.0.0.1
#define PORT      4433

class SSLConnection {
private:
  const char *server;
  const char *port;
  
  mbedtls_net_context server_fd;
  mbedtls_entropy_context entropy;
  mbedtls_ctr_drbg_context ctr_drbg;
  mbedtls_ssl_context ssl;
  mbedtls_ssl_config conf;
  
  SSLX509 *x509;
  
  bool connected;
  
  // Marked as unused
  mbedtls_ssl_session saved_session;
  unsigned char psk[MBEDTLS_PSK_MAX_LEN];
  size_t psk_len;
  
  void resetSession();
  
public:
  SSLConnection(const char* server, const char *port);
  ~SSLConnection();
  
  bool connect();
  bool setup();
  bool handshake();
  bool verifyCertificates();
  
  int write(const void *p_data, size_t p_len);
  int read(void *p_data, size_t p_len);
  
  bool reconnect();
  bool close();
  
  void saveSession(void);
  
};
