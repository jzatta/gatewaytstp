
#include "SSLConnection.h"
#include "glue.h"

#include <string.h>

#define mbedtls_printf printf_sgx


// #define DEBUG
#ifdef DEBUG
#define THRESHOLD_DEBUG 0
/*
 * Enabled if debug_level > 1 in code below
 */
static int my_verify( void *data, mbedtls_x509_crt *crt, int depth, uint32_t *flags )
{
  char buf[1024];
  ((void) data);
  
  mbedtls_printf( "\nVerify requested for (Depth %d):\n", depth );
  mbedtls_x509_crt_info( buf, sizeof( buf ) - 1, "", crt );
  mbedtls_printf( "%s", buf );
  
  if ( ( *flags ) == 0 )
    mbedtls_printf( "  This certificate has no flags\n" );
  else
  {
    mbedtls_x509_crt_verify_info( buf, sizeof( buf ), "  ! ", *flags );
    mbedtls_printf( "%s\n", buf );
  }
  
  return( 0 );
}
static void my_debug( void *ctx, int level,
                      const char *file, int line,
                      const char *str )
{
  const char *p, *basename;
  (void)(ctx);
  
  /* Extract basename from file */
  for ( p = basename = file; *p != '\0'; p++ )
    if ( *p == '/' || *p == '\\' )
      basename = p + 1;
    
    mbedtls_printf("%s:%04d: |%d| %s", basename, line, level, str );
}
#else
#define THRESHOLD_DEBUG 0
#endif












SSLConnection::SSLConnection(const char* _server, const char *_port) {
  int ret;
  server = _server;
  port = _port;
  psk_len = 0;
  connected = false;
  // Load certificates
  
  mbedtls_net_init(&server_fd);
  mbedtls_ssl_init(&ssl);
  mbedtls_ssl_config_init(&conf);
  memset(&saved_session, 0, sizeof( mbedtls_ssl_session ));
  mbedtls_ctr_drbg_init(&ctr_drbg);
  
  // DEBUG LEVEL TLS
  mbedtls_debug_set_threshold( THRESHOLD_DEBUG );
  // DEBUG LEVEL TLS
  /*
   * 0. Initialize the RNG and the session data
   */
  LL_LOG("Seeding the random number generator..." );
  mbedtls_entropy_init(&entropy);
  ret = mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy,NULL,0);
  if (ret != 0) {
    LL_CRITICAL(" mbedtls_ctr_drbg_seed returned -%#x", -ret);
    // ???
  }
  /*
   * 1. Load the trusted CA
   */
  x509 = SSLX509::getX509();
}

SSLConnection::~SSLConnection() {
  /*
  * Cleanup and exit
  */
  mbedtls_net_free(&server_fd);
  
  mbedtls_ssl_session_free(&saved_session);
  mbedtls_ssl_free(&ssl);
  mbedtls_ssl_config_free(&conf);
  mbedtls_ctr_drbg_free(&ctr_drbg);
  mbedtls_entropy_free(&entropy);
  
  SSLX509::destroy();
}

bool SSLConnection::connect(void) {
  int ret;
  if (connected == true) {
    return true;
  }
  /*
   * 2. Start the connection
   */
  
  LL_LOG("connecting to %s:%s:%s...", "TCP", server, port);
  ret = mbedtls_net_connect(&server_fd, server, port, MBEDTLS_NET_PROTO_TCP);
  if (ret != 0) {
    LL_CRITICAL(" mbedtls_net_connect returned -%#x", -ret);
    return false;
  }
  
  ret = mbedtls_net_set_block(&server_fd);
  if (ret != 0 ) {
    LL_CRITICAL(" net_set_(non)block() returned -%#x", -ret);
    return false;
  }
  
  if (!setup()) {
    return false;
  }
  if (!handshake()) {
    return false;
  }
  if (!verifyCertificates()) {
    return false;
  }
  connected = true;
  return true;
}

bool SSLConnection::setup(void) {
  int ret;
  /*
   * 3. Setup stuff
   */
  LL_LOG("Setting up the SSL/TLS structure...");
  
  ret = mbedtls_ssl_config_defaults(&conf, MBEDTLS_SSL_IS_CLIENT,
                      MBEDTLS_SSL_TRANSPORT_STREAM, MBEDTLS_SSL_PRESET_DEFAULT);
  if (ret != 0) {
    LL_CRITICAL("mbedtls_ssl_config_defaults returned -%#x", -ret);
    return false;
  }
  
  #ifdef DEBUG
  mbedtls_ssl_conf_verify(&conf, my_verify, NULL);
  #endif
  ret  = mbedtls_ssl_conf_max_frag_len(&conf, MBEDTLS_SSL_MAX_FRAG_LEN_NONE);
  if (ret != 0) {
    mbedtls_printf("  mbedtls_ssl_conf_max_frag_len returned %d\n\n", ret);
    return false;
  }
  
  
  mbedtls_ssl_conf_rng(&conf, mbedtls_ctr_drbg_random, &ctr_drbg);
  #ifdef DEBUG
  mbedtls_ssl_conf_dbg(&conf, my_debug, NULL);
  #endif
  
  mbedtls_ssl_conf_read_timeout(&conf, 0);
  mbedtls_ssl_conf_session_tickets(&conf, MBEDTLS_SSL_SESSION_TICKETS_ENABLED);
  mbedtls_ssl_conf_renegotiation(&conf, MBEDTLS_SSL_RENEGOTIATION_DISABLED);
  
  // Implicit:
  // mbedtls_ssl_conf_ca_chain(conf, &certsChain, &revokedChain);
  // mbedtls_ssl_conf_own_cert(conf, &clientCerts, &pkey);
  ret = x509->ssl_conf(&conf);
  if (ret != 0) {
    mbedtls_printf("  mbedtls_ssl_conf_own_cert returned %d\n\n", ret);
    return false;
  }
  
  const char *psk_identity = "Client_identity";
  ret = mbedtls_ssl_conf_psk(&conf, psk, psk_len,
                    (const unsigned char *) psk_identity, strlen(psk_identity));
  if (ret != 0) {
    mbedtls_printf("  mbedtls_ssl_conf_psk returned %d\n\n", ret);
    return false;
  }
  
  ret = mbedtls_ssl_setup(&ssl, &conf);
  if (ret != 0) {
    LL_CRITICAL("mbedtls_ssl_setup returned -%#x", -ret);
    return false;
  }
  
  ret = mbedtls_ssl_set_hostname(&ssl, server);
  if (ret != 0) {
    LL_CRITICAL("mbedtls_ssl_set_hostname returned %d\n\n", ret);
    return false;
  }
  
  mbedtls_ssl_set_bio(&ssl, &server_fd, mbedtls_net_send, mbedtls_net_recv,
                      mbedtls_net_recv_timeout);
  return true;
}

bool SSLConnection::handshake() {
  int ret;
  /*
   * 4. Handshake
   */
  LL_LOG( "Performing the SSL/TLS handshake" );
  ret = mbedtls_ssl_handshake(&ssl);
  while (ret != 0) {
    if (ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE) {
      LL_CRITICAL( "mbedtls_ssl_handshake returned -%#x", -ret );
      if ( ret == MBEDTLS_ERR_X509_CERT_VERIFY_FAILED )
        LL_CRITICAL(
          "Unable to verify the server's certificate. "
          "Either it is invalid,"
          "or you didn't set ca_file or ca_path "
          "to an appropriate value."
          "Alternatively, you may want to use "
          "auth_mode=optional for testing purposes." );
        return false;
    }
    ret = mbedtls_ssl_handshake(&ssl);
  }
  LL_LOG( "Hand shake succeeds: [%s, %s]",
         mbedtls_ssl_get_version( &ssl ), mbedtls_ssl_get_ciphersuite(&ssl));
  
  ret = mbedtls_ssl_get_record_expansion(&ssl);
  if (ret >= 0)
    LL_DEBUG("Record expansion is [%d]", ret);
  else
    LL_DEBUG("Record expansion is [unknown (compression)]");
  
  LL_LOG("  . Saving session for reuse..." );

  ret = mbedtls_ssl_get_session(&ssl, &saved_session);
  if (ret != 0) {
    LL_CRITICAL("mbedtls_ssl_get_session returned -%#x", -ret );
    return false;
  }

  LL_LOG("ok");
  
  LL_LOG("Maximum fragment length is [%u]",
         (unsigned int) mbedtls_ssl_get_max_frag_len(&ssl));
  return true;
}

bool SSLConnection::verifyCertificates() {
  int flags;
  /*
   * 5. Verify the server certificate
   */
  LL_LOG( "Verifying peer X.509 certificate..." );
  flags = mbedtls_ssl_get_verify_result(&ssl);
  if (flags != 0) {
    char vrfy_buf[512];
    
    mbedtls_printf(" failed\n");
    
    mbedtls_x509_crt_verify_info(vrfy_buf, sizeof( vrfy_buf ), "  ! ", flags);
    
    mbedtls_printf("%s\n", vrfy_buf);
  }
  else  
    LL_LOG("X.509 Verifies");
  
  #ifdef DEBUG
  char buf[512];
  if (mbedtls_ssl_get_peer_cert( &ssl ) != NULL) {
    LL_DEBUG( "Peer certificate information");
    mbedtls_x509_crt_info( (char *) buf, sizeof( buf ) - 1, "|-",
                                            mbedtls_ssl_get_peer_cert( &ssl ) );
    mbedtls_printf("%s\n", buf);   
  }
  #endif
  return true;
}

int SSLConnection::write(const void *pv_data, size_t p_len) {
  int ret, written, frags;
  const uint8_t *p_data = (const uint8_t *)pv_data;
  
  if (!connected) {
    return -1;
  }
  
  /*
   * 6. Write to the server
   */
  for (written = 0, frags = 0; written < p_len; written += ret, frags++) {
    do {
      ret = mbedtls_ssl_write(&ssl, p_data + written, p_len - written);
      if (ret < 0) {
        if (ret != MBEDTLS_ERR_SSL_WANT_READ &&
                                            ret != MBEDTLS_ERR_SSL_WANT_WRITE) {
          mbedtls_printf("  mbedtls_ssl_write returned -%#x\n", -ret);
          connected = false;
          return ret;
        }
      }
    } while (ret <= 0);
  }
  #ifdef DEBUG
  LL_LOG("%d bytes written in %d fragments", written, frags);
  LL_LOG("%s", (char*) p_data);
  
  mbedtls_debug_print_buf(&ssl, 0, __FILE__, __LINE__, "bytes written: ",
                          p_data, written);
  #endif
  return written;
}

int SSLConnection::read(void *pv_data, size_t p_len) {
  int ret, readed;
  uint8_t *p_data = (uint8_t *)pv_data;
  
  if (!connected) {
    return -1;
  }
  
  /*
   * 7. Read the HTTP response
   */
  readed = 0;
  memset(p_data, 0, p_len);
  
  ret = mbedtls_ssl_read(&ssl, p_data, p_len);
  
  while (ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE){
    ret = mbedtls_ssl_read(&ssl, p_data, p_len);
  }
  
  if (ret <= 0) {
    switch(ret) {
      case MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY:
        mbedtls_printf(" connection was closed gracefully\n" );
        break;
      case 0:
      case MBEDTLS_ERR_NET_CONN_RESET:
        mbedtls_printf(" connection was reset by peer\n" );
        ret = -1;
        break;
      default:
        mbedtls_printf(" mbedtls_ssl_read returned -0x%x\n", -ret );
        break;
    }
    connected = false;
    return ret;
  }
  
  readed = ret;
  
  #ifdef DEBUG
  LL_LOG("get %d bytes ending with %x", readed, p_data[readed-1]);
  mbedtls_debug_print_buf(&ssl, 0, __FILE__, __LINE__, "response", p_data,
                                                                        readed);
  #endif
  
  return readed;
}

bool SSLConnection::reconnect() {
  int ret;
  /*
   * 9. Reconnect?
   */
  if (connected == true) {
    return true;
  }
  
  mbedtls_net_free( &server_fd );
  
  mbedtls_printf( "  . Reconnecting with saved session..." );
  
  ret = mbedtls_ssl_session_reset(&ssl);
  if (ret != 0) {
    mbedtls_printf( "  mbedtls_ssl_session_reset returned -%#x", -ret );
    return false;
  }
  
  ret = mbedtls_ssl_set_session(&ssl, &saved_session);
  if (ret != 0) {
    mbedtls_printf( "  mbedtls_ssl_conf_session returned %d\n\n", ret );
    return false;
  }
  
  ret = mbedtls_net_connect(&server_fd, server, port, MBEDTLS_NET_PROTO_TCP);
  if (ret != 0) {
    mbedtls_printf( "  mbedtls_net_connect returned -%#x", -ret );
    return false;
  }
  
  ret = mbedtls_net_set_block( &server_fd );
  if (ret != 0) {
    mbedtls_printf( "  net_set_(non)block() returned -%#x",
                    -ret );
    return false;
  }
  ret = mbedtls_ssl_handshake(&ssl);
  while (ret != 0) {
    if (ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE) {
      mbedtls_printf( "  mbedtls_ssl_handshake returned -%#x", -ret );
      return false;
    }
    ret = mbedtls_ssl_handshake(&ssl);
  }
  
  connected = true;
  return true;
}

bool SSLConnection::close() {
  int ret;
  if (connected == false) {
    return true;
  }
  connected = false;
  /*
  * 8. Done, cleanly close the connection
  */
  do {
    ret = mbedtls_ssl_close_notify(&ssl);
    if (ret != MBEDTLS_ERR_SSL_WANT_WRITE && ret < 0) {
      
    }
  } while (ret == MBEDTLS_ERR_SSL_WANT_WRITE);
  
  LL_LOG( "closed %s:%s", server, port);
  return true;
}
